﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;

namespace iOSToolkit {
	public class iOSToolkitWrapper : MonoBehaviour {
		#if UNITY_IOS && !UNITY_EDITOR
		#region INTEROP DECLARATIONS
		[DllImport ("__Internal")]
		private static extern UInt64 getTotalDiskSpace ();

		[DllImport ("__Internal")]
		private static extern UInt64 getFreeDiskSpace ();

		[DllImport ("__Internal")]
		private static extern string getUserLocalesArrayAsString ();

		[DllImport ("__Internal")]
		private static extern bool checkTouchIdSupport ();

		[DllImport ("__Internal")]
		private static extern void beginTouchIdAuthentication(AuthenticationCompleteCallback callback);
		#endregion

		#region EVENTS AND DELEGATES
		public delegate void AuthenticationCompleteCallback(bool success);
		#endregion

		/// <summary>
		/// Gets the total disk size of the iOS device.
		/// </summary>
		/// <returns>The total disk size in bytes as an unsigned int64; an int is not big enough.</returns>
		public static UInt64 GetTotalDiskSize() {
			return getTotalDiskSpace ();
		}

		/// <summary>
		/// Gets the free disk sapce of the iOS device.
		/// </summary>
		/// <returns>The free disk sapce in bytes as an unsigned int64; an int is not big enough. </returns>
		public static UInt64 GetFreeDiskSapce() {
			return getFreeDiskSpace ();
		}

		/// <summary>
		/// Gets the user locales chosen by the user.
		/// </summary>
		/// <returns>The user locales as a string. (Not serialized into array)</returns>
		public static string GetUserLocales() {
			return getUserLocalesArrayAsString ();
		}

		public static bool CheckTouchIdSupport() {
			Debug.Log (checkTouchIdSupport ());
			return checkTouchIdSupport ();
		}

		public static void BeginTouchIdAuthentication(AuthenticationCompleteCallback callback) {
			beginTouchIdAuthentication(callback);
		}

#endif
	}
}
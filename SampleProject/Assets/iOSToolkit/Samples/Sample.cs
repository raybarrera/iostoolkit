﻿using UnityEngine;
using System.Collections;

namespace iOSToolkit {
	public class Sample : MonoBehaviour {
		#if UNITY_IOS && !UNITY_EDITOR
		// Use this for initialization
		void Start () {
			Debug.Log(iOSToolkitWrapper.GetFreeDiskSapce ());
			Debug.Log(iOSToolkitWrapper.GetTotalDiskSize ());
			Debug.Log(iOSToolkitWrapper.GetUserLocales ());
		}
		#endif
	}
}

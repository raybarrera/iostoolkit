﻿using UnityEngine;
using System.Collections;
using System;

public class TouchIdSample : MonoBehaviour {
	#if UNITY_IOS && !UNITY_EDITOR
	// Use this for initialization
	void Start () {
		Debug.Log("Begin auth");
		iOSToolkit.iOSToolkitWrapper.BeginTouchIdAuthentication(OnAuthenticationComplete);
	}


	[AOT.MonoPInvokeCallback(typeof(iOSToolkit.iOSToolkitWrapper.AuthenticationCompleteCallback))]
	public static void OnAuthenticationComplete(bool success){
		Debug.Log ("AUTHENTICATION STATUS: " + success);
	}
	#endif
}

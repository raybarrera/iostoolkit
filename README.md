# Welcome to iOS Toolkit!
Maintained by @ray.barrera

# What is iOS Toolkit?
iOS Toolkit provides access to native iOS functionality from within Unity.
No "SendMessage" and no "Listeners". iOS Toolkit uses interop functionality to call iOS Objective-C++ code directly from C#.

------------------------------------------------
# Getting Started
------------------------------------------------
Look at the included sample project for more information. More functionality will be added as needed.


------------------------------------------------
# Versions
------------------------------------------------

# 0.1.0 - 06/16/2017
[> > Download < <](/Releases/iOSToolkit_0.1.0.unitypackage)

Lightweight initial release.

Features:
- (API) Get device max disk size.
- (API) Get device available storage size.
- (API) Get user-selected locale settings.

Known issues:
- None. Please report any issues using the "issues" tab in gitlab.
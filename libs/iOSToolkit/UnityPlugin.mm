//
//  UnityPlugin.m
//  iOSToolkit
//
//  Created by Ray Barrera on 6/8/17.
//  Copyright © 2017 Ray Barrera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnityPlugin.h"
#import "DeviceLanguage.h"
#import "DeviceStorage.h"
#import "BiometricsAuthentication.h"

@implementation UnityPlugin

extern "C" {
    
    char* getUserLocalesArrayAsString() {
        NSString * string = [[DeviceLanguage getLanguages] description];
        return copyString([string UTF8String]);
    }
    
    uint64_t getFreeDiskSpace() {
        return [DeviceStorage getFreeDiskSpace];
    }
    
    uint64_t getTotalDiskSpace () {
        return [DeviceStorage getTotalDiskSize];
    }
    
    bool checkTouchIdSupport() {
        return [BiometricsAuthentication checkTouchIdSupport];
    }
    
    void beginTouchIdAuthentication(authenticationCallback authenticationCallback) {
        [BiometricsAuthentication beginTouchIdAuthentication : authenticationCallback];
    }
}

char* copyString(const char* string) {
    if(string == NULL) {
        return NULL;
    }
    
    char* result = (char*) malloc(strlen(string) + 1);
    strcpy(result, string);
    return result;
}

@end

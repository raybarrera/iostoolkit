//
//  DeviceLangauge.m
//  iOSToolkit
//
//  Created by Ray Barrera on 6/8/17.
//  Copyright © 2017 Ray Barrera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeviceLanguage.h"

@implementation DeviceLanguage

+ (NSArray*)getLanguages{
    NSArray *languages = nil;
    NSError *error = nil;
    languages = [NSLocale preferredLanguages];
    if(languages) {
        return languages;
    } else {
        NSLog(@"%@", [error localizedDescription]);
        return nil;
    }
}

@end

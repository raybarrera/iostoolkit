//
//  DeviceStorage.m
//  iOSToolkit
//
//  Created by Ray Barrera on 6/8/17.
//  Copyright © 2017 Ray Barrera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeviceStorage.h"

@implementation DeviceStorage

+ (uint64_t)getFreeDiskSpace{
    uint64_t totalFreeSpace = 0;
    __autoreleasing NSError * error = nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath: [paths lastObject]error: &error];
    if(dictionary) {
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
    } else {
        
    }
    return totalFreeSpace;
}

+ (uint64_t)getTotalDiskSize{
    uint64_t totalDiskSpace = 0;
    __autoreleasing NSError * error = nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath: [paths lastObject]error: &error];
    if(dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemSize];
        totalDiskSpace = [fileSystemSizeInBytes unsignedLongLongValue];
    } else {
        NSLog(@"ERROR");
    }
    return totalDiskSpace;
}

@end

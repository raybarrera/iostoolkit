//
//  DeviceLanguage.h
//  iOSToolkit
//
//  Created by Ray Barrera on 6/8/17.
//  Copyright © 2017 Ray Barrera. All rights reserved.
//

#ifndef DeviceLanguage_h
#define DeviceLanguage_h

@interface DeviceLanguage : NSObject

+ (NSArray*) getLanguages;

@end

#endif /* DeviceLanguage_h */

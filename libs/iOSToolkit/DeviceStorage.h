//
//  DeviceStorage.h
//  iOSToolkit
//
//  Created by Ray Barrera on 6/8/17.
//  Copyright © 2017 Ray Barrera. All rights reserved.
//

#ifndef DeviceStorage_h
#define DeviceStorage_h

@interface DeviceStorage : NSObject

+ (uint64_t)getFreeDiskSpace;
+ (uint64_t)getTotalDiskSize;

#endif /* DeviceStorage_h */
@end

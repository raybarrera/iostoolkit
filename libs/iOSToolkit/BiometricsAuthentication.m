//
//  BiometricsAuthentication.m
//  iOSToolkit
//
//  Created by Ray Barrera on 6/16/17.
//  Copyright © 2017 Ray Barrera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BiometricsAuthentication.h"
#import <LocalAuthentication/LAContext.h>


@implementation BiometricsAuthentication

+ (void)beginTouchIdAuthentication : (authenticationCallback) callback {
    LAContext *context = [[LAContext alloc] init];
    NSString *reason = @"reason";
    NSError *error = nil;
    BOOL *success = false;
    
    if([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error: &error]){
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason: reason
                          reply:^(BOOL success, NSError *error) {
                              if(success){
                                  NSLog(@"success");
                                  success = true;
                              }
                          }];
    } else {
        NSLog(@"INFO: Touch ID not available!");
    }
    if(callback != NULL){
        callback(success);
    }
}

+(BOOL)checkTouchIdSupport {
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    BOOL *success = false;
    if([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error: &error]){
        *success = TRUE;
    }
    return *success;
}

@end

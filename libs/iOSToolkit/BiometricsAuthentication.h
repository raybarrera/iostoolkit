//
//  BiometricsAuthentication.h
//  iOSToolkit
//
//  Created by Ray Barrera on 6/16/17.
//  Copyright © 2017 Ray Barrera. All rights reserved.
//

#ifndef BiometricsAuthentication_h
#define BiometricsAuthentication_h
#import <Foundation/Foundation.h>

@interface BiometricsAuthentication : NSObject

typedef bool (*authenticationCallback)();

+(BOOL)checkTouchIdSupport;
+(void)beginTouchIdAuthentication : (authenticationCallback) callback;

@end

#endif /* BiometricsAuthentication_h */
